// In Table


<html>
    <head>
    </head>
    
    <body>
<?php 
@foreach($log as $k=>$c)
    <tr id="tr_{{$c->id}}">
        <td> {{++$k}}</td>
        <td class="responsive-td">
            <div class="btn-group btn-group-solid">
                <button type="button" class="btn yellow modal_button edit_button" data-toggle="modal"  data-target="#createModal" data-img="{{$c->image}}" value="2" data-status="{{$c->id}}" data-category_id="{{$c->category_id}}" data-trid="tr#tr_{{$c->id}}" data-sub="{{$c->id}}"><i class="fa fa-pencil-square-o" id="edit_button"></i> Edit</button>
                <button type="button" class="btn red modal_button delete_button" data-toggle="modal"  data-target="#small" value="3"  data-status="{{$c->id}}" data-sub="{{$c->id}}"><i class="fa fa-trash"></i> Delete</button>
            </div>
        </td>
    </tr>
@endforeach
?>


    </body

</html>
    

//  Modal Dialog 

<html>
    <head>
    </head>
    
    <body>
        
        
        
        <div class="modal fade" id="small" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title"> <b class="text-uppercase"> <span id="modal-heading">Event Delete!</span> </b></h4>
                    </div>
                    <div class="modal-body">
                        <h5> <b>Are you sure about this ?</b></h5>
                        <input type="text" id="confirm_subcategory">
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-block"  data-dismiss="modal" id="confirm_delete_subcategory" style="color:#fff">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        
    
    </body

</html>
    
    
    
    
    // script
    <script>
        $(document).ready(function(){
        
                $(document).on('click','.delete_button',function(){
                    var id = $(this).data('sub');
                    $('#confirm_subcategory').val(id);
                    // alert(id);
                });
                
                
                $(document).on('click','#confirm_delete_subcategory',function(){
                    var id = $('#confirm_subcategory').val();
                    $.ajax({
                        type:"delete",
                        url:"{{route('subcategory.delete')}}",
                        data:{
                            id : id,
                            _token: "{{csrf_token()}}"
                        },
                        success:function(data){
                            console.log(data);
                            if (data == 0){
                                $('#tr_'+id).hide();
                                toastr.success(" Delete Successfully");
                            }
                        }
                        ,
                        error:function(res){
    
                        }
                    })
                });
                
                
        };
        
        </script>

    // Route
        <?php
        
        Route::delete('delete-subcategory',['as'=>'subcategory.delete','uses'=>'DashboardController@deleteSubcategory']);
        
        ?>
    
    // In controller
    
    <?php
        public function deleteSubcategory(Request $request){
            $data = SubCategory::findOrFail($request->id);
            $data->delete();
            $Deleted_data = SubCategory::find($request->id);
            if($Deleted_data){
                $status = 1;
            }else{
                $status = 0;
            }
            return $status;
        }
    ?>
